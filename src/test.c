#include "test.h"

static void print_start(size_t i);
static void print_result(bool result);
static struct block_header* block_get_header(void* contents);

bool test_1(void* heap){
  print_start(1);
  bool result = false;
  void* temp = _malloc(0x10);
  debug_heap(stdout, heap);
  if (temp){
    struct block_header* temp_header = block_get_header(temp);
    if (!temp_header->is_free){
      result = true;
    }
  }
  _free(temp);
  print_result(result);
  return result;
}

bool test_2(void* heap){
  print_start(2);
  bool result = false;
  void* temp_1 = _malloc(0x10);
  void* temp_2 = _malloc(0x10);
  void* temp_3 = _malloc(0x10);
  void* temp_4 = _malloc(0x10);
  debug_heap(stdout, heap);
  _free(temp_1);
  debug_heap(stdout, heap);
  struct block_header* temp_header = block_get_header(temp_1);
  if (temp_header->is_free){
    result = true;
  }
  _free(temp_2);
  _free(temp_3);
  _free(temp_4);
  print_result(result);
  return result;
}

bool test_3(void* heap){
  print_start(3);
  bool result = false;
  void* temp_1 = _malloc(0x10);
  void* temp_2 = _malloc(0x10);
  void* temp_3 = _malloc(0x10);
  void* temp_4 = _malloc(0x10);
  debug_heap(stdout, heap);
  _free(temp_1);
  _free(temp_2);
  debug_heap(stdout, heap);
  struct block_header* temp_header_1 = block_get_header(temp_1);
  struct block_header* temp_header_2 = block_get_header(temp_2);
  struct block_header* temp_header_3 = block_get_header(temp_3);
  struct block_header* temp_header_4 = block_get_header(temp_4);
  if (temp_header_1->is_free
    && temp_header_2->is_free
    && !temp_header_3->is_free
    && !temp_header_4->is_free){
    result = true;
  }
  _free(temp_3);
  _free(temp_4);
  print_result(result);
  return result;
}

bool test_4(void* heap){
  print_start(4);
  bool result = false;
  void* temp_1 = _malloc(0x90);
  debug_heap(stdout, heap);
  void* temp_2 = _malloc((0x100 - 0x90) * 2);
  debug_heap(stdout, heap);
  struct block_header* temp_header = block_get_header(temp_2);
  if (!temp_header->is_free){
    result = true;
  }
  _free(temp_1);
  _free(temp_2);
  print_result(result);
  return result;
}

bool test_5(void* heap){
  print_start(5);
  bool result = false;
  void* temp_1 = _malloc(0x1000);
  debug_heap(stdout, heap);

  struct block_header* temp_header_1 = block_get_header(temp_1);
  if(temp_header_1->next){
    temp_header_1 = temp_header_1->next;
  }
  void* busy = (void*)(((uint8_t*) temp_header_1) + size_from_capacity(temp_header_1->capacity).bytes);
  busy = mmap(busy, 0x90, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, 0, 0);
  *((struct block_header *) busy) = (struct block_header) {
                .next = NULL,
                .capacity = capacity_from_size((block_size) {.bytes = 0x90}),
                .is_free = true
  };
  debug_heap(stdout, busy);

  void* temp_2 = _malloc(0x1100);
  debug_heap(stdout, heap);
  struct block_header* temp_header_2 = block_get_header(temp_2);
  if (!temp_header_2->is_free){
    result = true;
  }
  _free(temp_1);
  _free(temp_2);
  print_result(result);
  return result;
}

bool run_all_tests(void* heap){
  if(test_1(heap)
    && test_2(heap)
    && test_3(heap)
    && test_4(heap)
    && test_5(heap)){
    return true;
  }
  return false;
}

static void print_start(size_t i){
  printf("\nTest %zu\n", i);
}

static void print_result(bool result){
  if (result == true){
    printf("\nTest passed\n");
  }
  else{
    printf("\nTest failed\n");
  }
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}
