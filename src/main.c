#include "main.h"
#include "test.h"

int main(){
  void* heap = heap_init(0x100);
  run_all_tests(heap);
  return 0;
}
