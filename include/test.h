#ifndef _TEST_
#define _TEST_

#include <stdarg.h>
#define _DEFAULT_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"


//returns bool array of un/pass test
//and
//prints the results
bool test_1(void* heap);
bool test_2(void* heap);
bool test_3(void* heap);
bool test_4(void* heap);
bool test_5(void* heap);
bool run_all_tests(void* heap);

#endif
